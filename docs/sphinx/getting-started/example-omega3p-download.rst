Downloading Omega3P Results
===========================

The contents of the job folder can be downloaded from the NERSC machine to the local file system for visualization and local postprocessing. :program:`ModelBuilder for ACE3P` also supports remote visualization of job data without requiring download. See :ref:`Example: Remote visualization`.


.. rst-class:: step-number

11\. Download Job Directory

Once the job is complete, you can download the job directory on NERSC including the ``omega3p_results`` subdirectory. Make sure the first (only) row in the jobs table is selected, then in the details section below, find the download toolbar to the right of the :guilabel:`Remote Directory` field. Click on that button to start the download process. The model (.ncdf) file will
be downloaded from NERSC to the project assets folder and the mode files will be downloaded from NERSC to the path shown in the :guilabel:`Download Directory` field.

.. image:: images/download-toolbutton-highlight.png
    :align: center

|
