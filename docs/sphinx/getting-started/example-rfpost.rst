Example: RF Postprocessing
==========================

.. image:: images/rfPost-outText.jpg
    :align: center

In this example, we will detail the steps requisite to perform an :program:`RF-postprocessing` analysis of a previous :program:`Omega3P` simulation. This example builds on the previous example for creating a project and running an :program:`Omega3P` simulation. Please ensure you have completed that example before moving on. Note that this procedure also applies to the postprocessing of :program:`S3P` and :program:`T3P` analyses.

.. toctree::
    :maxdepth: 1

    rfpost/example-rfpost-open.rst
    rfpost/example-rfpost-init.rst
    rfpost/example-rfpost-view.rst

.. note::
    Screenshots in this section were taken on macOS with dark mode enabled.
