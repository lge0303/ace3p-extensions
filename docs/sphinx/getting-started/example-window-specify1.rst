Specify S3P Analysis
====================

In this stage, we will set up an :program:`S3P` analysis for one frequency. All these steps will be performed in the Attribute Editor panel.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

1\. Select ``S3P`` for the Analysis in the Modules tab

.. image:: images/window-modules.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

2\. Specify the surface types in the Boundary Conditions tab

.. image:: images/window-boundary-conditions.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

3\. Specify material properties in the Materials tab

We are going to define material attributes for each of the two element blocks in the model The ``volume 1`` element block represents the cavity geometry and ``volume 2`` represents a window in the center of the cavity.

1. Click on the ``New`` button to create the first material attribute. Assign this to the first element block by clicking ``volume 1`` in the :guilabel:`Available` list (at the bottom of the panel), and press the left arrow button to assign it to the first material.  After clicking the arrow, ``volume 1`` should appear in the :guilabel:`Current` list (on the left-hand side). For ``volume 1`` we are using the default values for permittivity and permeability, so you we don't need to edit those fields.)

2. Click on the ``New`` button to create the second material attribute and assign this to the second element block by clicking ``volume 2`` in the :guilabel:`Available` list using the left arrow button to move it to the :guilabel:`Current` list. For ``volume 2`` set the :guilabel:`Relative Permittivity (Epsilon)` to 9.37

Once finished, the panel should look like this:

.. image:: images/window-materials.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

4\. Specify parameters in the Analysis tab

Set the Frequency Information :guilabel:`Mode` to :guilabel:`Waveguide Frequency`. Below that is a field :guilabel:`Waveguide Frequency (Hz)` for entering the frequency value; set it ``2.856e9`` as shown here:

.. image:: images/window-analysis.png
    :align: center

|
