View S3P Results
================

.. rst-class:: step-number

1\. Download job directory

Once the job has completed, you can download the results using the "Download" tool button in the :guilabel:`Simulation Jobs` panel, as decribed in :ref:`Downloading Omega3P Results`.

.. rst-class:: step-number

2\. Display the Results Data

When the download is finished, you can view the EM field data by clicking the "Display" tool button as described in :ref:`Viewing Omega3P Results`.

.. image:: images/window-result1.png
    :align: center

|
