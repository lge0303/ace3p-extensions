***************
Getting Started
***************

To introduce :program:`ModelBuilder for ACE3P`, here are some walkthroughs and examples that illustrate the basic features for generating :program:`ACE3P` input files, running :program:`ACE3P` simulations on computing resources at NERSC, and downloading the results for visualization and further analysis.

.. toctree::
    :maxdepth: 1

    installing.rst
    example-omega3p.rst
    example-rfpost.rst
    example-remoteviz.rst
    example-window.rst
    example-rfgun.rst
    advanced-topics.rst
