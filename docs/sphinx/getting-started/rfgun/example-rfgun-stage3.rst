Stage 3: Omega3P (deformed mesh)
=================================

The electromagnetic heating simulated in Stage 2 causes structural deformation to the cavity which in turn causes a shift in its resonant frequency. We will be adding a third stage to the project to calculate the new resonance point for the deformed cavity mesh. To begin, we *could* download the deformed mesh from the NERSC machine and use that as the input to a third stage, but instead we will just tell modelbuilder to use the deformed cavity mesh without downloading it.


.. rst-class:: step-number

1\. Create Stage 3

To add a third stage to the project, go to the :guilabel:`ACE3P` menu and select the :guilabel:`Add Stage...` item to open the dialog for specifying the new stage. In the dialog, set the :guilabel:`Mesh` field to ``Select From Project`` and in the :guilabel:`Select Mesh` field select ``RfGunVacuum.gen``. Click the :guilabel:`Apply` button to add the stage to the project.

.. rst-class:: step-number


2\. Modules Tab

In the :guilabel:`Modules` tab, set the :guilabel:`Analysis` to ``Omega3P``.

Below that, check the box labeled :guilabel:`Deformed Mesh File` to show a text field for a remote mesh file.

.. rst-class:: with-border
.. image:: ../images/rfgun-analysis3.png
    :align: center

|

Next click the tool button to the left of the text field (arrow) to open a popup widget with a table listing :program:`TEM3P` jobs from previous stages. Select the one row in the table and click the :guilabel:`Select` button.

.. image:: ../images/rfgun-tem3p-jobs.png
    :align: center

|

In response, modelbuilder will close the popup and paste the path to the :guilabel:`Deformed Mesh File` field.

.. rst-class:: with-border
.. image:: ../images/rfgun-analysis3b.png
    :align: center

|


.. rst-class:: step-number

3\. Boundary Conditions Tab

In the :guilabel:`Boundary Conditions` tab, duplicate the settings used in Stage 1, specifically, set side sets 1 and 5 to ``Electric``, side sets 2 through 4 to ``Magnetic``, and side set 6 to ``Exterior``.

.. rst-class:: with-border
.. image:: ../images/rfgun-boundary-conditions.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

4\. Analysis Tab

In the :guilabel:`Analysis` tab, change :guilabel:`Frequency Shift (Hz)` to ``2.8e9``.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

5\. Submit Job

Submit the :program:`ACE3P` job by going to the :guilabel:`ACE3P` menu and selecting the :guilabel:`Submit Analysis Job…` item to bring up the job-submit dialog. Enter your info in the :guilabel:`Project Repository` field. You can also set the :guilabel:`Number of Cores` to ``32`` so that :program:`Omega3P` will use all of the computing node CPU resources.

Most importantly, click in the :guilabel:`Results directory` field and change the text to ``omega3p_results_deformed``.

.. important:: Be sure to change the :guilabel:`Results directory` field, otherwise you will overwrite the results computed in Stage 1.

When everything is set, click the :guilabel:`Apply` button to submit the job.


.. rst-class:: step-number

6\. Download Results

After the job is submitted, you can track its progress in the :guilabel:`Simulation Jobs` panel. When the job is complete, you can then download and display the results using the tool buttons as described for Stages 1 and 2.
