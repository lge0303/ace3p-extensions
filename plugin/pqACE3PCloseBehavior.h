//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PCloseBehavior_h
#define pqACE3PCloseBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for closing a project.
class pqACE3PCloseReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqACE3PCloseReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqACE3PCloseReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqACE3PCloseBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqACE3PCloseBehavior* instance(QObject* parent = nullptr);
  ~pqACE3PCloseBehavior() override;

  /** \brief Closes project, returning true if NOT canceled by the user
   *
   * In other words, the return value reports if the close action
   * was "not cancelled".
   */
  bool closeProject();

Q_SIGNALS:
  void projectClosed();

protected:
  pqACE3PCloseBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqACE3PCloseBehavior);
};

#endif
