//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_plugin_pqACE3PExportBehavior_h
#define smtk_simulation_ace3p_plugin_pqACE3PExportBehavior_h

#include "smtk/simulation/ace3p/Project.h"
#include "smtk/simulation/ace3p/qt/qtTypeDeclarations.h"

#include "smtk/PublicPointerDefs.h"
#include "smtk/operation/Operation.h"

#include "pqReaction.h"

#include <QObject>
#include <QString>

#include "nlohmann/json.hpp"

class qtProgressDialog;

namespace smtk
{
namespace simulation
{
namespace ace3p
{
class qtNewtJobSubmitter;
}
} // namespace simulation
} // namespace smtk

/// A reaction for writing a resource manager's state to disk.
class pqACE3PExportReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqACE3PExportReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqACE3PExportReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqACE3PExportBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqACE3PExportBehavior* instance(QObject* parent = nullptr);
  ~pqACE3PExportBehavior() override;

  void exportProject();

Q_SIGNALS:
  void jobSubmitted(nlohmann::json jobRecord);

protected Q_SLOTS:
  void reviewExportFile(
    const smtk::operation::Operation::Parameters exportParams,
    const smtk::operation::Operation::Result result);

protected:
  pqACE3PExportBehavior(QObject* parent = nullptr);

  void submitJob(
    const smtk::operation::Operation::Parameters exportParams,
    const smtk::operation::Operation::Result result);

  // Asks user to save project if modified
  bool cleanProject(smtk::project::ProjectPtr project);

  // Checks if results subfolder overlaps with a previous stage.
  // Returns true if the subfolder name is ok.
  bool checkResultsFolder(
    const smtk::operation::Operation::Parameters exportParams,
    std::shared_ptr<smtk::simulation::ace3p::Project> project,
    int stageIndex) const;

  // Keeps copy of export parameters in memory after operation completes
  smtk::attribute::ResourcePtr m_paramsResource;

  smtk::simulation::ace3p::qtNewtJobSubmitter* m_jobSubmitter;
  qtProgressDialog* m_progressDialog = nullptr;

private:
  Q_DISABLE_COPY(pqACE3PExportBehavior);
};

#endif
