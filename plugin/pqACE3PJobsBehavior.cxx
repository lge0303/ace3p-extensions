//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqACE3PJobsBehavior.h"

#include "plugin/pqACE3PJobsPanel.h"
#include "smtk/simulation/ace3p/qt/qtJobsWidget.h"
#include "smtk/simulation/ace3p/qt/qtProgressDialog.h"

#include "pqCoreUtilities.h"

#include <QDebug>

//-----------------------------------------------------------------------------
pqACE3PJobsReaction::pqACE3PJobsReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

void pqACE3PJobsReaction::onTriggered()
{
  pqACE3PJobsBehavior* behavior = pqACE3PJobsBehavior::instance();
  behavior->showWidget();
}

//-----------------------------------------------------------------------------
static pqACE3PJobsBehavior* g_instance = nullptr;

pqACE3PJobsBehavior::pqACE3PJobsBehavior(QObject* parent)
  : Superclass(parent)
{
  QWidget* mainWidget = pqCoreUtilities::mainWidget();
  qtProgressDialog* progressDialog = new qtProgressDialog(mainWidget, 0, 0, "Progress");
  m_jobsWidget = new smtk::simulation::ace3p::qtJobsWidget(progressDialog, mainWidget);
}

pqACE3PJobsBehavior* pqACE3PJobsBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqACE3PJobsBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqACE3PJobsBehavior::~pqACE3PJobsBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

void pqACE3PJobsBehavior::hideJobsPanel()
{
  this->setPanelVisible(false);
}

void pqACE3PJobsBehavior::setPanelVisible(bool bVisible)
{
  m_jobsPanel->setVisible(bVisible);
}

void pqACE3PJobsBehavior::showWidget()
{
  m_jobsWidget->show();
}

void pqACE3PJobsBehavior::onJobOverwritten(const QString& jobId)
{
  m_jobsWidget->onJobOverwritten(jobId);
}
