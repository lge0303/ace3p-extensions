//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PJobsBehavior_h
#define pqACE3PJobsBehavior_h

// ParaView includes
#include "pqReaction.h"

// Qt includes
#include <QObject>

class pqACE3PJobsPanel;

namespace smtk
{
namespace simulation
{
namespace ace3p
{
class qtJobsWidget;
}
} // namespace simulation
} // namespace smtk

/** \brief A reaction for showing the Jobs widget. */
class pqACE3PJobsReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /** \brief Constructor. Parent cannot be NULL. */
  pqACE3PJobsReaction(QAction* parent);

protected:
  /** \brief Called when the action is triggered. */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqACE3PJobsReaction)
};

/** \brief Add a menu item for opening the Jobs widget. */
class pqACE3PJobsBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqACE3PJobsBehavior* instance(QObject* parent = nullptr);
  ~pqACE3PJobsBehavior() override;

  /** \brief Sets pointer for the Jobs Panel (used to change its visibility). */
  void setJobsPanelPointer(pqACE3PJobsPanel* jobsPanel) { m_jobsPanel = jobsPanel; }

  /** \brief Shows/hides Stages widget panel. */
  void setPanelVisible(bool bVisible);

  /** \brief Opens or raises the Jobs widget. */
  void showWidget();

  smtk::simulation::ace3p::qtJobsWidget* jobsWidget() { return m_jobsWidget; }

public Q_SLOTS:
  /** \brief Notifies jobs widget that a job's data has been overwritten */
  void onJobOverwritten(const QString& jobId);

  /** \brief Hides the Jobs Panel - a special case of setPanelVisible(). */
  void hideJobsPanel();

protected:
  pqACE3PJobsBehavior(QObject* parent = nullptr);

  smtk::simulation::ace3p::qtJobsWidget* m_jobsWidget;

private:
  Q_DISABLE_COPY(pqACE3PJobsBehavior);

  pqACE3PJobsPanel* m_jobsPanel;
};

#endif
