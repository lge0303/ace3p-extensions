//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqACE3PRemoteParaViewBehavior.h"

#ifdef _WIN32
#include "plugin/remote/modelbuilder-cori-win_cpp.h"
#else
#include "plugin/remote/modelbuilder-cori_cpp.h"
#endif
#include "smtk/simulation/ace3p/qt/qtMessageDialog.h"

#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"
#include "pqServerConfiguration.h"
#include "pqServerConnectReaction.h"
#include "pqServerManagerModel.h"
#include "vtkPVXMLElement.h"
#include "vtkPVXMLParser.h"
#include "vtkProcessModule.h"

#include "vtkNew.h"

#include <QAction>
#include <QDebug>
#include <QString>
#include <QStringList>

//=============================================================================
pqACE3PRemoteParaViewMenu::pqACE3PRemoteParaViewMenu(QWidget* parent)
  : QMenu("Launch Remote ParaView Server", parent)
{
  this->buildMenu();
}

//-----------------------------------------------------------------------------
void pqACE3PRemoteParaViewMenu::buildMenu()
{
  this->clear();

  // Add Cori
  QAction* action = this->addAction("Cori", this, &pqACE3PRemoteParaViewMenu::onTriggered);
  action->setData("cori");

  // Future: Add Perlmutter
}

//-----------------------------------------------------------------------------
void pqACE3PRemoteParaViewMenu::onTriggered()
{
  QAction* action = qobject_cast<QAction*>(this->sender());
  if (action == nullptr)
  {
    qCritical() << "Internal error: sender null or not a QAction instance";
    return;
  }
  QString machine = action->data().toString();

  // Check if already connected
  auto behavior = pqACE3PRemoteParaViewBehavior::instance();
  pqServer* server = behavior->remoteServer(machine);
  if (server == nullptr)
  {
    behavior->startRemoteServer(machine);
  }
  else
  {
    auto* mainWidget = pqCoreUtilities::mainWidget();
    QString msg = QString("Already connected to remote ParaView server on %1").arg(machine);
    QMessageBox::critical(mainWidget, "Connected", msg);
  }
}

//=============================================================================
static pqACE3PRemoteParaViewBehavior* g_instance = nullptr;

//-----------------------------------------------------------------------------
pqACE3PRemoteParaViewBehavior::pqACE3PRemoteParaViewBehavior(QObject* parent)
  : Superclass(parent)
{
  // Listen for remote server added signals (for debug only)
  pqServerManagerModel* model = pqApplicationCore::instance()->getServerManagerModel();

  // Listen for remote server disconnect signals and update m_serverMap
  QObject::connect(model, &pqServerManagerModel::serverRemoved, [this](pqServer* server) {
    QString name = server->getResource().configuration().name();
    qDebug() << "Server Removed:" << name << server->isRemote();
    for (auto it = m_serverMap.begin(); it != m_serverMap.end(); ++it)
    {
      if (it.value()->getResource().configuration().name() == name)
      {
        qDebug() << "Server removed. Updating m_serverMap";
        m_serverMap.erase(it);
        break;
      }
    }
  });
}

//-----------------------------------------------------------------------------
pqACE3PRemoteParaViewBehavior* pqACE3PRemoteParaViewBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqACE3PRemoteParaViewBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

//-----------------------------------------------------------------------------
pqACE3PRemoteParaViewBehavior::~pqACE3PRemoteParaViewBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

//-----------------------------------------------------------------------------
pqServer* pqACE3PRemoteParaViewBehavior::remoteServer(const QString& machine) const
{
  auto iter = m_serverMap.find(machine);
  if (iter == m_serverMap.end())
  {
    return nullptr;
  }
  return *iter;
}

//-----------------------------------------------------------------------------
bool pqACE3PRemoteParaViewBehavior::startRemoteServer(const QString& machine)
{
  // Check for --multi-servers
  if (!vtkProcessModule::GetProcessModule()->GetMultipleSessionsSupport())
  {
    auto* mainWindow = pqCoreUtilities::mainWidget();
    QString msg = "Modelbuilder cannot launch remote paraview servers because it was not"
                  " started with the --multi-servers command line option.";
    QMessageBox::critical(mainWindow, "Missing --multi-servers option", msg);
    return false;
  }

  pqServerManagerModel* model = pqApplicationCore::instance()->getServerManagerModel();
  QObject::connect(model, &pqServerManagerModel::serverAdded, [&](pqServer* server) {
    if (server->isRemote())
    {
      qDebug() << "Server added:" << machine << "Updating m_serverMap";
      m_serverMap[machine] = server;
      pqActiveObjects::instance().setActiveServer(server);

      // Wait for active server events to finish, then emit our signal
      pqCoreUtilities::processEvents();
      Q_EMIT this->serverAdded(server);
      m_showDisconnectDialog = true;

      // And disconnect until next time
      QObject::disconnect(model, &pqServerManagerModel::serverAdded, this, nullptr);

      // these connections notify user when the remote server is about to time out
      QObject::connect(
        server,
        &pqServer::fiveMinuteTimeoutWarning,
        this,
        &pqACE3PRemoteParaViewBehavior::serverTimeoutIn5Mins);
      QObject::connect(
        server,
        &pqServer::finalTimeoutWarning,
        this,
        &pqACE3PRemoteParaViewBehavior::serverTimeoutIn1Min);
      QObject::connect(
        server,
        &pqServer::serverSideDisconnected,
        this,
        &pqACE3PRemoteParaViewBehavior::serverTimeout);
    }
  });

  // Create server configuration and connect
  vtkNew<vtkPVXMLParser> parser;
  int result = parser->Parse(cori_pvsc().c_str());
  vtkPVXMLElement* root = parser->GetRootElement();
  pqServerConfiguration config(root);

  pqServerConnectReaction::connectToServerUsingConfiguration(config);
  return true;
}

// warn user when the server connection is about to expire
void pqACE3PRemoteParaViewBehavior::serverTimeoutIn5Mins()
{
  if (m_messageDialog == nullptr)
  {
    m_messageDialog = new qtMessageDialog(pqCoreUtilities::mainWidget());
  }
  m_messageDialog->setIcon(QMessageBox::Warning);
  m_messageDialog->setWindowTitle("Remote Server Warning");
  m_messageDialog->setText("The remote server connection will timeout in 5 mimutes.");
  m_messageDialog->setAutoClose(true);
  m_messageDialog->setAutoCloseDelay(15);
  m_messageDialog->show();
}

// warn user when the server connection is about to expire
void pqACE3PRemoteParaViewBehavior::serverTimeoutIn1Min()
{
  if (m_messageDialog == nullptr)
  {
    m_messageDialog = new qtMessageDialog(pqCoreUtilities::mainWidget());
  }
  m_messageDialog->setIcon(QMessageBox::Warning);
  m_messageDialog->setWindowTitle("Remote Server Warning");
  m_messageDialog->setText("The remote server connection will timeout in 1 mimute.");
  m_messageDialog->setAutoClose(true);
  m_messageDialog->setAutoCloseDelay(15);
  m_messageDialog->show();
}

// notify user when the server connection has expired
void pqACE3PRemoteParaViewBehavior::serverTimeout()
{
  if (!m_showDisconnectDialog)
  {
    return;
  }

  if (m_messageDialog == nullptr)
  {
    m_messageDialog = new qtMessageDialog(pqCoreUtilities::mainWidget());
  }
  m_messageDialog->setIcon(QMessageBox::Critical);
  m_messageDialog->setWindowTitle("Remote Server Disconnected");
  QString message;
  QTextStream qs(&message);
  qs << "The remote server connection has timed out and, as a result,"
     << " the modelbuilder application might become unstable."
     << " Users should save their current project and close modelbuilder as soon as possible.";
  m_messageDialog->setText(message);
  m_messageDialog->show();

  m_showDisconnectDialog = false;
}
