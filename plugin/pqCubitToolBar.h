//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef ace3p_extension_plugin_pqCubitToolBar_h
#define ace3p_extension_plugin_pqCubitToolBar_h

#include "smtk/PublicPointerDefs.h"

#include <QString>
#include <QToolBar>
#include <QtGlobal> // qint64

class pqCubitToolBar : public QToolBar
{
  Q_OBJECT
  using Superclass = QToolBar;

public:
  pqCubitToolBar(QWidget* parent = nullptr);
  ~pqCubitToolBar() override;

protected Q_SLOTS:
  void onLaunch();

protected:
  void askUserForPath(const QString& headline, const QString& details, QString& result) const;
  bool checkCubitPath(const QString& path, QString& reason) const;
  bool getCubitPath(QString& path) const;
  bool getWorkspacePath(QString& path) const;
  bool runCubit(const QString& path, const QString& workingDir);

  // Remember if we already started CUBIT before
  qint64 m_cubitPID;

private:
  Q_DISABLE_COPY(pqCubitToolBar);
};

#endif
