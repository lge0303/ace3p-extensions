<?xml version="1.0"?>
<!--Created by XmlV4StringWriter-->
<SMTK_AttributeResource Version="4">
  <!--**********  Category and Analysis Information ***********-->
  <Categories>
    <Cat>Rf-Postprocess</Cat>
  </Categories>
  <!--**********  Attribute Definitions ***********-->
  <Associations />
  <Definitions>
    <AttDef Type="RfPostprocess" Label="RfPostprocess" BaseType="" Version="0" Unique="false">
      <CategoryInfo Combination="All">
        <Include Combination="Any">
          <Cat>Rf-Postprocess</Cat>
        </Include>
      </CategoryInfo>
      <ItemDefinitions>
        <Group Name="RFField" Label="RFField" Version="0" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="FreqScanID" Label="FreqScanID" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <BriefDescription> For S3P only: frequency scan index</BriefDescription>
              <DefaultValue>0</DefaultValue>
            </Int>
            <Int Name="ModeID" Label="ModeID" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Int>
            <String Name="xsymmetry" Label="xsymmetry" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <BriefDescription> [none, electric, magnetic]</BriefDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="none">none</Value>
                <Value Enum="electric">electric</Value>
                <Value Enum="magnetic">magnetic</Value>
              </DiscreteInfo>
            </String>
            <String Name="ysymmetry" Label="ysymmetry" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <BriefDescription> [none, electric, magnetic]</BriefDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="none">none</Value>
                <Value Enum="electric">electric</Value>
                <Value Enum="magnetic">magnetic</Value>
              </DiscreteInfo>
            </String>
            <Double Name="gradient" Label="gradient" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>20000000</DefaultValue>
            </Double>
            <Double Name="cavityBeta" Label="cavityBeta" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <BriefDescription>for R/Q, V integral</BriefDescription>
              <DefaultValue>1</DefaultValue>
            </Double>
            <Int Name="reversePowerFlow" Label="reversePowerFlow" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <BriefDescription> [1=charging 0=decaying]</BriefDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="decaying">0</Value>
                <Value Enum="charging">1</Value>
              </DiscreteInfo>
            </Int>
            <Double Name="x0" Label="x0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <BriefDescription> x for gradient calc</BriefDescription>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y0" Label="y0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <BriefDescription> y for gradient calc</BriefDescription>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="z0" Label="z0" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <BriefDescription> set field at this z location when gradient&lt;0</BriefDescription>
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="gz1" Label="gz1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <BriefDescription> starting z for gradient calc</BriefDescription>
              <DefaultValue>-0.035000000000000003</DefaultValue>
            </Double>
            <Double Name="gz2" Label="gz2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <BriefDescription> starting z for gradient calc</BriefDescription>
              <DefaultValue>0.035000000000000003</DefaultValue>
            </Double>
            <Int Name="npoint" Label="npoint" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>1000</DefaultValue>
            </Int>
            <Int Name="fmnx" Label="fmnx" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>20</DefaultValue>
            </Int>
            <Int Name="fmny" Label="fmny" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>20</DefaultValue>
            </Int>
            <Int Name="fmnz" Label="fmnz" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100</DefaultValue>
            </Int>
          </ItemDefinitions>
        </Group>
        <Group Name="pointRoverQ" Label="pointRoverQ" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="modeID1" Label="modeID1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Int Name="modeID2" Label="modeID2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Double Name="x0" Label="x0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y0" Label="y0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="z0" Label="z0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="RoverQ" Label="RoverQ" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="modeID1" Label="modeID1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Int Name="modeID2" Label="modeID2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Double Name="x1" Label="x1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="x2" Label="x2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y1" Label="y1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="y2" Label="y2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="RoverQT" Label="RoverQT" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="modeID1" Label="modeID1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Int Name="modeID2" Label="modeID2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Double Name="x0" Label="x0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y0" Label="y0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="kickFactor" Label="kickFactor" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="modeID1" Label="modeID1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Int Name="modeID2" Label="modeID2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Double Name="x0" Label="x0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y0" Label="y0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="VFFT" Label="VFFT" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="rot180" Label="rot180" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="no rotation">0</Value>
                <Value Enum="model rotated 180deg about z">1</Value>
              </DiscreteInfo>
            </Int>
            <String Name="printGroup" Label="printGroup" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <BriefDescription> [nterm, ModeID]</BriefDescription>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="nterm">nterm</Value>
                <Value Enum="ModeID">ModeID</Value>
              </DiscreteInfo>
            </String>
            <Int Name="modeID1" Label="modeID1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Int Name="modeID2" Label="modeID2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Int Name="nterm" Label="nterm" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>4</DefaultValue>
            </Int>
            <Int Name="ntheta" Label="ntheta" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>16</DefaultValue>
            </Int>
            <Double Name="r0" Label="r0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.0050000000000000001</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="GBZFFT" Label="GBZFFT" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <String Name="filename" Label="filename" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>filename1</DefaultValue>
            </String>
            <Int Name="rot180" Label="rot180" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="no rotation">0</Value>
                <Value Enum="model rotated 180deg about z">1</Value>
              </DiscreteInfo>
            </Int>
            <Int Name="nterm" Label="nterm" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>4</DefaultValue>
            </Int>
            <Int Name="ntheta" Label="ntheta" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>16</DefaultValue>
            </Int>
            <Double Name="energy" Label="energy" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>50000000</DefaultValue>
            </Double>
            <Int Name="npoint" Label="npoint" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>300</DefaultValue>
            </Int>
            <Double Name="r0" Label="r0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.0050000000000000001</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="phase1" Label="phase1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-180</DefaultValue>
            </Double>
            <Double Name="phase2" Label="phase2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>180</DefaultValue>
            </Double>
            <Int Name="nphase" Label="nphase" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>19</DefaultValue>
            </Int>
          </ItemDefinitions>
        </Group>
        <Group Name="Multipole" Label="Multipole" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <String Name="filename" Label="filename" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>filename1</DefaultValue>
            </String>
            <Int Name="rot180" Label="rot180" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="no rotation">0</Value>
                <Value Enum="model rotated 180deg about z">1</Value>
              </DiscreteInfo>
            </Int>
            <Double Name="energy" Label="energy" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>50000000</DefaultValue>
            </Double>
            <Int Name="npoint" Label="npoint" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>300</DefaultValue>
            </Int>
            <Double Name="r0" Label="r0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.0050000000000000001</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="phase1" Label="phase1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-180</DefaultValue>
            </Double>
            <Double Name="phase2" Label="phase2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>180</DefaultValue>
            </Double>
            <Int Name="nphase" Label="nphase" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>19</DefaultValue>
            </Int>
          </ItemDefinitions>
        </Group>
        <Group Name="Track" Label="Track" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <String Name="filename" Label="filename" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>filename1</DefaultValue>
            </String>
            <Double Name="energy" Label="energy" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>50000000</DefaultValue>
            </Double>
            <Double Name="x0" Label="x0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y0" Label="y0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="rfphase" Label="rfphase" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Int Name="maxSteps" Label="maxSteps" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>3000</DefaultValue>
            </Int>
          </ItemDefinitions>
        </Group>
        <Group Name="TrackScan" Label="TrackScan" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <String Name="filename" Label="filename" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>filename1</DefaultValue>
            </String>
            <String Name="scanfilename" Label="scanfilename" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>scanfile1</DefaultValue>
            </String>
            <Int Name="itraj_out" Label="itraj_out" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Int>
            <Double Name="energy" Label="energy" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>50000000</DefaultValue>
            </Double>
            <Double Name="x0" Label="x0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y0" Label="y0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="phase1" Label="phase1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-180</DefaultValue>
            </Double>
            <Double Name="phase2" Label="phase2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>180</DefaultValue>
            </Double>
            <Int Name="nphase" Label="nphase" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>19</DefaultValue>
            </Int>
            <Int Name="maxSteps" Label="maxSteps" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>3000</DefaultValue>
            </Int>
          </ItemDefinitions>
        </Group>
        <Group Name="FieldMap" Label="FieldMap" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="nx" Label="nx" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>20</DefaultValue>
            </Int>
            <Int Name="ny" Label="ny" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>20</DefaultValue>
            </Int>
            <Int Name="nz" Label="nz" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>50</DefaultValue>
            </Int>
            <Double Name="x1" Label="x1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="x2" Label="x2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y1" Label="y1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="y2" Label="y2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="FieldMap_Serial" Label="FieldMap_Serial" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="nx" Label="nx" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>20</DefaultValue>
            </Int>
            <Int Name="ny" Label="ny" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>20</DefaultValue>
            </Int>
            <Int Name="nz" Label="nz" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>50</DefaultValue>
            </Int>
            <Double Name="x1" Label="x1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="x2" Label="x2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y1" Label="y1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="y2" Label="y2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="IMPACTMap" Label="IMPACTMap" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <String Name="filename" Label="filename" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>filename1</DefaultValue>
            </String>
            <Int Name="rot180" Label="rot180" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="no rotation">0</Value>
                <Value Enum="model rotated 180deg about z">1</Value>
              </DiscreteInfo>
            </Int>
            <Int Name="nx" Label="nx" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>20</DefaultValue>
            </Int>
            <Int Name="ny" Label="ny" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>20</DefaultValue>
            </Int>
            <Int Name="nz" Label="nz" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>50</DefaultValue>
            </Int>
            <Double Name="x1" Label="x1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="x2" Label="x2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y1" Label="y1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="y2" Label="y2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="OpenPMD_IMPACT" Label="OpenPMD_IMPACT" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="nx" Label="nx" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>20</DefaultValue>
            </Int>
            <Int Name="ny" Label="ny" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>20</DefaultValue>
            </Int>
            <Int Name="nz" Label="nz" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>50</DefaultValue>
            </Int>
            <Double Name="x1" Label="x1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="x2" Label="x2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y1" Label="y1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="y2" Label="y2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="FieldAtPoint" Label="FieldAtPoint" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Double Name="x0" Label="x0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y0" Label="y0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="z0" Label="z0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="ALLFieldAtPoint" Label="ALLFieldAtPoint" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="modeID1" Label="modeID1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Int Name="modeID2" Label="modeID2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Double Name="x0" Label="x0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y0" Label="y0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="z0" Label="z0" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="FieldOnLine" Label="FieldOnLine" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="npoint" Label="npoint" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>300</DefaultValue>
            </Int>
            <String Name="filename" Label="filename" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>filename1</DefaultValue>
            </String>
            <Double Name="rfphase" Label="rfphase" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="x1" Label="x1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="x2" Label="x2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y1" Label="y1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="y2" Label="y2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="ALLFieldOnLine" Label="ALLFieldOnLine" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="rot180" Label="rot180" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="no rotation">0</Value>
                <Value Enum="model rotated 180deg about z">1</Value>
              </DiscreteInfo>
            </Int>
            <Int Name="modeID1" Label="modeID1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Int Name="modeID2" Label="modeID2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Int Name="npoint" Label="npoint" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>300</DefaultValue>
            </Int>
            <String Name="filename" Label="filename" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>filename1</DefaultValue>
            </String>
            <Double Name="rfphase" Label="rfphase" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="x1" Label="x1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="x2" Label="x2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0</DefaultValue>
            </Double>
            <Double Name="y1" Label="y1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="y2" Label="y2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>0.001</DefaultValue>
            </Double>
            <Double Name="z1" Label="z1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="z2" Label="z2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="fieldOn2DBoundary" Label="fieldOn2DBoundary" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <String Name="filename" Label="filename" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>filename1</DefaultValue>
            </String>
            <Int Name="surfaceID" Label="surfaceID" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>6</DefaultValue>
            </Int>
          </ItemDefinitions>
        </Group>
        <Group Name="fieldOnSurface" Label="fieldOnSurface" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <String Name="filename" Label="filename" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>filename1</DefaultValue>
            </String>
            <Int Name="surfaceID" Label="surfaceID" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>6</DefaultValue>
            </Int>
            <String Name="output" Label="output" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <BriefDescription> [component, amplitude]</BriefDescription>
              <DiscreteInfo DefaultIndex="1">
                <Value Enum="component">component</Value>
                <Value Enum="amplitude">amplitude</Value>
              </DiscreteInfo>
            </String>
          </ItemDefinitions>
        </Group>
        <Group Name="maxFieldsOnSurface" Label="maxFieldsOnSurface" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="surfaceID" Label="surfaceID" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>6</DefaultValue>
            </Int>
            <Double Name="xmin" Label="xmin" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="xmax" Label="xmax" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="ymin" Label="ymin" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="ymax" Label="ymax" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="zmin" Label="zmin" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
            <Double Name="zmax" Label="zmax" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>100000000</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="powerThroughSurface" Label="powerThroughSurface" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="surfaceID" Label="surfaceID" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>6</DefaultValue>
            </Int>
          </ItemDefinitions>
        </Group>
        <Group Name="poyntingVector" Label="poyntingVector" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <String Name="filename" Label="filename" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>filename1</DefaultValue>
            </String>
            <Int Name="surfaceID" Label="surfaceID" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>6</DefaultValue>
            </Int>
          </ItemDefinitions>
        </Group>
        <Group Name="coaxPort" Label="coaxPort" Version="0" Optional="true" IsEnabledByDefault="false" NumberOfRequiredGroups="1">
          <CategoryInfo Inherit="true" Combination="All" />
          <ItemDefinitions>
            <Int Name="modeID1" Label="modeID1" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Int Name="modeID2" Label="modeID2" Version="0" NumberOfRequiredValues="1">
              <CategoryInfo Inherit="true" Combination="All" />
              <DefaultValue>-1</DefaultValue>
            </Int>
            <Group Name="port-info" Label="port-info" Version="0" NumberOfRequiredGroups="1" Extensible="true">
              <CategoryInfo Inherit="true" Combination="All" />
              <ComponentLabels CommonLabel="" />
              <ItemDefinitions>
                <Int Name="portID" Label="portID" Version="0" NumberOfRequiredValues="1">
                  <CategoryInfo Inherit="true" Combination="All" />
                </Int>
                <Double Name="porta" Label="porta" Version="0" NumberOfRequiredValues="1">
                  <CategoryInfo Inherit="true" Combination="All" />
                </Double>
                <Double Name="portb" Label="portb" Version="0" NumberOfRequiredValues="1">
                  <CategoryInfo Inherit="true" Combination="All" />
                </Double>
              </ItemDefinitions>
            </Group>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
