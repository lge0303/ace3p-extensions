<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>Omega3P</Cat>
    <Cat>S3P</Cat>
    <Cat>T3P</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="Tolerant" Label="Tolerant" Version="0">
      <ItemDefinitions>
        <Void Name="Tolerant" Label="Tolerant" Version="0"
          Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>S3P</Cat>
            <Cat>T3P</Cat>
            <Cat>Omega3P</Cat>
          </Categories>
        </Void>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="FEInfo" BaseType="" Version="0">
      <ItemDefinitions>
        <String Name="LinearSolver" Label="Linear Solver">
          <BriefDescription>Specifies the solver for solving the linear system used in t3p simulation</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>

          <ChildrenDefinitions>
            <String Name="Preconditioner">
              <BriefDescription>Type of Preconditioner to be used</BriefDescription>
              <Categories>
                <Cat>T3P</Cat>
              </Categories>
              <DiscreteInfo>
                <Value Enum="CHOLESKY">CHOLESKY</Value>
                <Value Enum="DIAGONAL">DIAGONAL</Value>
              </DiscreteInfo>
            </String>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Value Enum="MUMPS">MUMPS</Value>
            <Structure>
              <Value Enum="CG">CG</Value>
              <Items><Item>Preconditioner</Item></Items>
            </Structure>
          </DiscreteInfo>
        </String>
        <Double Name="MaxTime" Label="Maximum Time" Units="s">
          <BriefDescription>Total simulation time</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
         </Categories>
         <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
        </Double>
        <Double Name="DT" Label="Time Step" Units="s">
          <BriefDescription>(DT) Time step for advancing the Maxwell equation numerically in the time domain</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
          <RangeInfo><Min Inclusive="false">0</Min></RangeInfo>
        </Double>
        <Int Name="Order" Label="Global Order" Version="0" >
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
          <DefaultValue>2</DefaultValue>
          <Categories>
            <Cat>S3P</Cat>
            <Cat>T3P</Cat>
            <Cat>Omega3P</Cat>
          </Categories>
        </Int>
        <Void Name="EnableCurvedSurfaces" Label="Enable Curved Surfaces" Version="0" Optional="true" IsEnabledByDefault="true">
          <Categories>
            <Cat>S3P</Cat>
            <Cat>Omega3P</Cat>
            <Cat>T3P</Cat>
          </Categories>
        </Void>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="RegionHighOrder" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Int Name="RegionHighOrder" Label="Region High Order" Version="0" >
          <Categories>
            <Cat>Omega3P</Cat>
          </Categories>
          <DefaultValue>2</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="MovingWindow" BaseType="" Version="0">
      <ItemDefinitions>
        <Group Name="MovingWindow" Label="Automatic Moving Window" Version="0"
          Optional="true" IsEnabledByDefault="false">
          <BriefDescription>Enables moving window technique in calculating short-range wakefield</BriefDescription>
          <ItemDefinitions>
            <Int Name="Order" Label="Region Order" Version="0" >
              <Categories>
                <Cat>T3P</Cat>
              </Categories>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
              <DefaultValue>2</DefaultValue>
            </Int>
            <Double Name="Back" Label="Back Distance" Version="0" Units="m">
              <BriefDescription>Distance from the back of the bunch to the back end of the window</BriefDescription>
              <Categories>
                <Cat>T3P</Cat>
              </Categories>
              <DefaultValue>1.0</DefaultValue>
            </Double>
            <Double Name="Front" Label="Front Distance" Version="0" Units="m">
              <BriefDescription>Distance from the front of the bunch to the front end of the window</BriefDescription>
              <Categories>
                <Cat>T3P</Cat>
              </Categories>
              <DefaultValue>1.0</DefaultValue>
            </Double>
            <Double Name="StructureEnd" Label="Structure End" Version="0" Units="m">
              <BriefDescription>The length of the model in the Z direction</BriefDescription>
              <Categories>
                <Cat>T3P</Cat>
              </Categories>
              <DefaultValue>1.0</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="FrequencyInfo" Label="Frequency Information" BaseType="" Version="0">
      <ItemDefinitions>
        <Int Name="NumEigenvalues" Label="Number of eigenmodes searched" Version="0" >
          <Categories>
            <Cat>Omega3P</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>
        <Double Name="FrequencyShift" Label="Frequency Shift" Version="0" Units="Hz">
          <Categories>
            <Cat>Omega3P</Cat>
          </Categories>
          <DefaultValue>1.0e9</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <String Name="FrequencyMode" Label="Mode">
          <Categories><Cat>S3P</Cat></Categories>
          <ChildrenDefinitions>
            <Double Name="WaveguideFrequency" Label="Waveguide Frequency" Version="0" Units="Hz">
              <DefaultValue>1.0e9</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="StartingFrequency" Label="Starting Frequency" Version="0" Units="Hz">
              <DefaultValue>1.0e9</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="EndingFrequency" Label="Ending Frequency" Version="0" Units="Hz">
            <DefaultValue>1.0e9</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="FrequencyInterval" Label="Frequency Interval" Version="0" Units="Hz">
              <DefaultValue>1.0e9</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Waveguide Frequency">waveguide</Value>
              <Items>
                <Item>WaveguideFrequency</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Frequency Scan">scan</Value>
              <Items>
                <Item>StartingFrequency</Item>
                <Item>EndingFrequency</Item>
                <Item>FrequencyInterval</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Omega3PSolver">
      <Categories>
        <Cat>Omega3P</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="Mode" Label="Solver Mode">
          <ChildrenDefinitions>
            <Double Name="TargetFrequencyMin" Label="Target Frequency Min" />
            <Double Name="TargetFrequencyMax" Label="Target Frequency Max" />
            <Double Name="TargetFrequencyImag" Label="Target Frequency Imaginary" />
            <Int Name="MaxIterations" Label="Max Iterations">
              <DefaultValue>500</DefaultValue>
            </Int>
            <Double Name="ApproxTolerance" Label="Approx Tolerance">
              <DefaultValue>1.0e-12</DefaultValue>
            </Double>
            <Double Name="ApproxMaxDegree" Label="Approx Max Degree">
              <DefaultValue>100.0</DefaultValue>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Baseline">default</Value>
            <Structure>
              <Value Enum="Nonlinear - CORK">cork</Value>
              <Items>
                <Item>TargetFrequencyMin</Item>
                <Item>TargetFrequencyMax</Item>
                <Item>TargetFrequencyImag</Item>
                <Item>MaxIterations</Item>
                <Item>ApproxTolerance</Item>
                <Item>ApproxMaxDegree</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="PostProcess" Label="Post Process" BaseType="" Version="0">
      <ItemDefinitions>
        <Group Name="Toggle" Label="Write Post Process Files" BaseType=""
          Version="1" Optional="true" IsEnabledByDefault="true">
          <ItemDefinitions>
            <String Name="ModeFilePrefix" Label="Mode Files Prefix" BaseType="" Version="0"
              Optional="true" IsEnabledByDefault="false">
              <BriefDescription>Prefix to use in labeling output *.mod files</BriefDescription>
              <DefaultValue>mode</DefaultValue>
              <Categories>
                <Cat>S3P</Cat>
                <Cat>Omega3P</Cat>
              </Categories>
            </String>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="CheckPoint" Label="Check Point">
      <Categories>
        <Cat>T3P</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="Action">
          <ChildrenDefinitions>
            <Int Name="Ntimesteps" Label="Interval" Units="Time Steps">
              <DefaultValue>1000</DefaultValue>
            </Int>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="1">
            <Value>none</Value>
            <Structure>
              <Value>restart</Value>
              <Items>
                <Item>Ntimesteps</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

  </Definitions>
</SMTK_AttributeResource>
