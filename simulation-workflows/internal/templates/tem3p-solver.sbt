<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>TEM3P-Eigen</Cat>
    <Cat>TEM3P-Harmonic</Cat>
    <Cat>TEM3P-Elastic</Cat>
    <Cat>TEM3P-Thermal</Cat>
    <Cat>TEM3P-Linear-Thermal</Cat>
    <Cat>TEM3P-Nonlinear-Thermal</Cat>
  </Categories>
  <Definitions>
    <AttDef Type="ElasticOrder" Label="Order" Version="0">
      <ItemDefinitions>
        <!-- Enable ReferenceT for both elastic and thermal analysis-->
        <Double Name="ReferenceT" Label="Reference Temperature" CategoryCheckMode="All" Version="0">
          <Categories>
            <Cat>TEM3P-Elastic</Cat> 
            <Cat>TEM3P-Thermal</Cat>
          </Categories>
        </Double>
        <Int Name="BasisOrder" Label="Basis Order" Version="0">
          <BriefDescirption>The order of finite elements</BriefDescirption>
          <Categories>
            <Cat>TEM3P-Elastic</Cat>
            <Cat>TEM3P-Eigen</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <Void Name="CurvedSurfaces" Label="Curved Surfaces" Optional="true" IsEnabledByDefault="true" Version="0">
          <BriefDescription>Use curved surfaces to better approximate the geometry</BriefDescription>
          <Categories>
            <Cat>TEM3P-Elastic</Cat>
            <Cat>TEM3P-Eigen</Cat>
          </Categories>
        </Void>
        <String Name="Type" Label="Linear Solver Type" Version="0">
          <Categproes>
            <Cat>TEM3P-Elastic</Cat>
          </Categproes>
          <BriefDescription>Type of solver</BriefDescription>
          <DiscreteInfo>
            <Value>CG</Value>
            <Value>GMRES</Value>
            <Value>MUMPS</Value>
          </DiscreteInfo>
        </String>
        <String Name="Preconditioner" Label="Preconditioner" Version="0">
          <BriefDescription>Type of preconditioner</BriefDescription>
          <DiscreteInfo>
            <Value>DIAGONAL</Value>
            <Value>CHOLESKY</Value>
            <Value>SSOR</Value>
            <Value>ILU</Value>
          </DiscreteInfo>
        </String>
        <Double Name="AbsoluteTolerance" Label="Absolute Tolerance" Version="0">
          <BriefDescription>Absolute convergence tolerance</BriefDescription>
          <DefaultValue>1.0e-18</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="Tolerance" Label="Tolerance" Version="0">
          <BriefDescription>Relative convergence tolerance</BriefDescription>
          <DefaultValue>1.0e-10</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Int Name="MaxIterations" Label="Max Iterations" Version="0">
          <BriefDescription>Maximum number of iterations when the solver process terminates even if the tolerance has not been reached</BriefDescription>
          <DefaultValue>100000</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ElasticLinearSolver" Label="Linear Solver" Version="0">
      <Categories>
        <Cat>TEM3P-Elastic</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="Type" Label="Linear Solver Type" Version="0">
          <Categproes>
            <Cat>TEM3P-Elastic</Cat>
          </Categproes>
          <BriefDescription>Type of solver</BriefDescription>
          <DiscreteInfo>
            <Value>CG</Value>
            <Value>GMRES</Value>
            <Value>MUMPS</Value>
          </DiscreteInfo>
        </String>
        <String Name="Preconditioner" Label="Preconditioner" Version="0">
          <BriefDescription>Type of preconditioner</BriefDescription>
          <DiscreteInfo>
            <Value>DIAGONAL</Value>
            <Value>CHOLESKY</Value>
            <Value>SSOR</Value>
            <Value>ILU</Value>
          </DiscreteInfo>
        </String>
        <Double Name="AbsoluteTolerance" Label="Absolute Tolerance" Version="0">
          <BriefDescription>Absolute convergence tolerance</BriefDescription>
          <DefaultValue>1.0e-18</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="Tolerance" Label="Tolerance" Version="0">
          <BriefDescription>Relative convergence tolerance</BriefDescription>
          <DefaultValue>1.0e-10</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Int Name="MaxIterations" Label="Max Iterations" Version="0">
          <BriefDescription>Maximum number of iterations when the solver process terminates even if the tolerance has not been reached</BriefDescription>
          <DefaultValue>100000</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ThermalOrder" Label="Order" Version="0">
      <ItemDefinitions>
        <Int Name="BasisOrder" Label="Basis Order" Version="0">
          <BriefDescirption>The order of finite elements</BriefDescirption>
          <Categories>
            <Cat>TEM3P-Thermal</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <Void Name="CurvedSurfaces" Label="Curved Surfaces" Optional="true" IsEnabledByDefault="true" Version="0">
          <BriefDescription>Use curved surfaces to better approximate the geometry</BriefDescription>
          <Categories>
            <Cat>TEM3P-Thermal</Cat>
          </Categories>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ThermalLinearSolver" Label="Linear Solver" Version="0">
      <Categories>
        <Cat>TEM3P-Thermal</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="Type" Label="Linear Solver Type" Version="0">
          <Categproes>
            <Cat>TEM3P-Elastic</Cat>
          </Categproes>
          <BriefDescription>Type of solver</BriefDescription>
          <DiscreteInfo>
            <Value>CG</Value>
            <Value>GMRES</Value>
            <Value>MUMPS</Value>
          </DiscreteInfo>
        </String>
        <String Name="Preconditioner" Label="Preconditioner" Version="0">
          <BriefDescription>Type of preconditioner</BriefDescription>
          <DiscreteInfo>
            <Value>DIAGONAL</Value>
            <Value>CHOLESKY</Value>
            <Value>SSOR</Value>
            <Value>ILU</Value>
          </DiscreteInfo>
        </String>
        <Double Name="AbsoluteTolerance" Label="Absolute Tolerance" Version="0">
          <BriefDescription>Absolute convergence tolerance</BriefDescription>
          <DefaultValue>1.0e-18</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="Tolerance" Label="Tolerance" Version="0">
          <BriefDescription>Relative convergence tolerance</BriefDescription>
          <DefaultValue>1.0e-10</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Int Name="MaxIterations" Label="Max Iterations" Version="0">
          <BriefDescription>Maximum number of iterations when the solver process terminates even if the tolerance has not been reached</BriefDescription>
          <DefaultValue>100000</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="PicardSolver" Label="Picard Solver" Version="0">
      <Categories>
        <Cat>TEM3P-Nonlinear-Thermal</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="Solver" Label="Picard Solver Type" Version="0">
          <BriefDescription>Type of solver</BriefDescription>
          <DiscreteInfo DefaultIndex="0">
            <Value>CG</Value>
          </DiscreteInfo>
        </String>
        <String Name="Preconditioner" Label="Preconditioner" Version="0">
          <BriefDescription>Type of preconditioner</BriefDescription>
          <DiscreteInfo>
            <Value>DIAGONAL</Value>
            <Value>CHOLESKY</Value>
            <Value>SSOR</Value>
            <Value>ILU</Value>
          </DiscreteInfo>
        </String>
        <Double Name="AbsoluteTolerance" Label="Absolute Tolerance" Version="0">
          <BriefDescription>Absolute convergence tolerance</BriefDescription>
          <DefaultValue>1.0e-18</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="Tolerance" Label="Tolerance" Version="0">
          <BriefDescription>Relative convergence tolerance</BriefDescription>
          <DefaultValue>1.0e-10</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Int Name="MaxIterations" Label="Max Iterations" Version="0">
          <BriefDescription>Maximum number of iterations when the solver process terminates even if the tolerance has not been reached</BriefDescription>
          <DefaultValue>100000</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="NonlinearSolver" Label="Nonlinear Solver" Version="0">
      <Categories>
        <Cat>TEM3P-Nonlinear-Thermal</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="Type" Label="Nonlinear Solver Type" Version="0">
          <BriefDescription>Type of solver</BriefDescription>
          <DiscreteInfo DefaultIndex="0">
            <Value>Newton</Value>
          </DiscreteInfo>
        </String>
        <Int Name="PicardIteration" Label="Picard Iterations" Version="0">
          <BriefDescription>Number of iterations before switching from Picard to Newton update</BriefDescription>
          <DefaultValue>10</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Int Name="MaxIterations" Label="Max Iterations" Version="0">
          <BriefDescription>Maximum number of iterations when the solver process terminates even if the tolerance has not been reached</BriefDescription>
          <DefaultValue>40</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Double Name="AbsTolerance" Label="Absolute Tolerance" Version="0">
          <BriefDescription>Absolute convergence tolerance</BriefDescription>
          <DefaultValue>1.0e-10</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="RelTolerance" Label="Relative Tolerance" Version="0">
          <BriefDescription>Relative convergence tolerance</BriefDescription>
          <DefaultValue>1.0e-8</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="EigenSolver" Label="EigenSolver" Version="0">
      <Categories>
        <Cat>TEM3P-Eigen</Cat>
      </Categories>
      <ItemDefinitions>
        <Int Name="NumEigenvalues" Label="Number of eigenvalues" Version="0">
          <BriefDescription>The number of eigenmodes searched</BriefDescription>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Double Name="FrequencyShift" Label="Frequency Shift" Version="0" Units="Hz">
          <BriefDescription>The frequency above which the eigenmodes are calculated</BriefDescription>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="HarmonicAnalysis" Label="Harmonic Analysis" Version="0">
      <Categories>
        <Cat>TEM3P-Harmonic</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="Frequency" Label="Harmonic Frequencies" Version="0">
          <ChildrenDefinitions>
            <Double Name="SingleFrequency" Label="Single" Version="0" Units="Hz">
              <BriefDescription>The frequency at which the harmonic response is calculated</BriefDescription>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="Start" Label="Starting Frequency" Version="0" Units="Hz">
              <BriefDescription>Start frequency</BriefDescription>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="End" Label="Ending Frequency" Version="0" Units="Hz">
              <BriefDescription>End frequency</BriefDescription>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="Interval" Label="Interval" Version="0" Units="Hz">
              <BriefDescription>Frequency sweep interval</BriefDescription>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Single Frequency">single</Value>
              <Items>
                <Item>SingleFrequency</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Scan Frequencies">scan</Value>
              <Items>
                <Item>Start</Item>
                <Item>End</Item>
                <Item>Interval</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
        <Group Name="Damping" Label="Damping" Optional="true" IsEnabledByDefault="false" Version="0">
          <ItemDefinitions>
            <Double Name="StiffnessDamping" Label="Stiffness Damping" Version="0">
              <BriefDescription>Stiffness-proportional damping coefficient</BriefDescription>
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <Double Name="MassDamping" Label="Mass Damping" Version="0">
              <BriefDescription>Mass-proportional damping coefficient</BriefDescription>
              <DefaultValue>0.0</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
