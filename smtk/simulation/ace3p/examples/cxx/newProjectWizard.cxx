//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/ace3p/qt/qtNewProjectWizard.h"

#include "smtk/simulation/ace3p/Metadata.h"
#include "smtk/simulation/ace3p/Registrar.h"

// SMTK includes
#include "smtk/attribute/Registrar.h"
#include "smtk/model/Registrar.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Registrar.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Registrar.h"
#include "smtk/resource/Manager.h"
#include "smtk/session/vtk/Registrar.h"

#include <QApplication>
#include <QDir>
#include <QString>
#include <QTextStream>

#include <iostream>

int main(int argc, char* argv[])
{
  QApplication app(argc, argv);

  // Create smtk managers
  smtk::resource::ManagerPtr resourceManager = smtk::resource::Manager::create();
  smtk::attribute::Registrar::registerTo(resourceManager);
  smtk::model::Registrar::registerTo(resourceManager);
  smtk::session::vtk::Registrar::registerTo(resourceManager);

  smtk::operation::ManagerPtr operationManager = smtk::operation::Manager::create();
  smtk::operation::Registrar::registerTo(operationManager);
  smtk::attribute::Registrar::registerTo(operationManager);
  smtk::session::vtk::Registrar::registerTo(operationManager);

  smtk::project::ManagerPtr projectManager =
    smtk::project::Manager::create(resourceManager, operationManager);
  smtk::project::Registrar::registerTo(projectManager);
  smtk::simulation::ace3p::Registrar::registerTo(projectManager);

// Application must set workflows directory
#ifndef WORKFLOWS_SOURCE_DIR
#error WORKFLOWS_SOURCE_DIR must be defined
#endif
  smtk::simulation::ace3p::Metadata::WORKFLOWS_DIRECTORY = WORKFLOWS_SOURCE_DIR;

  // Display the widget
  auto* w = new smtk::simulation::ace3p::qtNewProjectWizard();
  w->setProjectManager(projectManager);

  QString path;
  QTextStream qs(&path);
  qs << QDir::homePath() << "/modelbuilder/projects";
  w->setProjectLocation(path);
  w->setMeshFileDirectory(QDir::homePath());
  w->show();

  int retval = app.exec();
  return retval;
}
