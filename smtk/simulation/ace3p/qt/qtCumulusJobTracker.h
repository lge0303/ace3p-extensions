//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtCumulusJobTracker_h
#define smtk_simulation_ace3p_qt_qtCumulusJobTracker_h

#include <QObject>
#include <QString>

#include "smtk/simulation/ace3p/qt/Exports.h"

#include "smtk/simulation/ace3p/qt/qtAbstractJobTracker.h"

class QNetworkReply;

namespace smtk
{
namespace simulation
{
namespace ace3p
{

/* \brief Manages cumulus I/O for tracking job progress.
 *
 * Accepts jobs to track and makes girder/cumulus requests to get status
 * at periodic intervals. Emits signal for each status update.
 * Stops tracking job when its status is terminal (complete or error).
 * Also supports single-shot update
 *
 */
class SMTKACE3PQTEXT_EXPORT qtCumulusJobTracker : public qtAbstractJobTracker
{
  Q_OBJECT
  using Superclass = qtAbstractJobTracker;

public:
  qtCumulusJobTracker(QObject* parent = nullptr);
  ~qtCumulusJobTracker() override;

  // Gets status for each job.
  bool pollOnce() override;

protected Q_SLOTS:
  void onCumulusError(const QString& msg, QNetworkReply* networkReply);
  void onCumulusReply();
  void onLogin();

protected:
  // Make job request to girder/cumulus server
  void requestJob(int index) override;

  // Checks if timer should be started for next polling interval.
  // The pollNow option will do the first poll immediately.
  bool setNextPoll(bool pollNow = false) override;

private:
  class Internal;
  Internal* m_internal;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk
#endif
