//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "qtMessageDialog.h"

#include <QAbstractButton>
#include <QApplication>
#include <QBuffer>
#include <QCommonStyle>
#include <QCoreApplication>
#include <QMutex>
#include <QProgressDialog>
#include <QTimer>

// minimal constructor
qtMessageDialog::qtMessageDialog(QWidget* parent)
  : QMessageBox(parent)
{
  initialize();
}

// constructor
qtMessageDialog::qtMessageDialog(
  Icon icon,
  const QString& title,
  const QString& text,
  StandardButtons buttons,
  QWidget* parent,
  Qt::WindowFlags flag)
  : QMessageBox(icon, title, text, buttons, parent, flag)
{
  initialize();
}

// common initialization
void qtMessageDialog::initialize()
{
  setWindowTitle("Information");
  m_autocloseTimer = new QTimer(this);
  QObject::connect(
    m_autocloseTimer, &QTimer::timeout, this, &qtMessageDialog::processAutoCloseTimer);

  m_countdownButton = QMessageBox::Ok;
  m_bAutoClose = false;
  m_autoCloseDelay = 0;
}

// destructor
qtMessageDialog::~qtMessageDialog()
{
  ;
}

void qtMessageDialog::showEvent(QShowEvent* event)
{
  QRect parentRect(parentWidget()->mapToGlobal(QPoint(0, 0)), parentWidget()->size());
  this->move(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), parentRect).topLeft());

  QWidget::showEvent(event);
}

int qtMessageDialog::exec()
{
  startAutocloseTimer();

  return QMessageBox::exec();
}

void qtMessageDialog::show()
{
  startAutocloseTimer();

  QMessageBox::show();
}

void qtMessageDialog::startAutocloseTimer()
{
  if (m_bAutoClose)
  {
    if (this->buttons().size() == 0)
    {
      this->setStandardButtons(m_countdownButton);
    }

    m_remainingAutoCloseSeconds = m_autoCloseDelay;
    QAbstractButton* button = this->button(m_countdownButton);
    m_originalButtonText = button->text();
    m_autocloseTimer->start(1000);
    processAutoCloseTimer(); // call manually one time to set inital countdown text
  }
}

// sets the dialog to automatically close upon progress bar completion
void qtMessageDialog::setAutoClose(bool bAutoClose)
{
  m_bAutoClose = bAutoClose;
}

// imposes a delay after the completion of a run, before auto-close is triggered
void qtMessageDialog::setAutoCloseDelay(uint delay)
{
  m_autoCloseDelay = delay;
}

// process auto-close timer ticks
void qtMessageDialog::processAutoCloseTimer()
{
  m_remainingAutoCloseSeconds -= 1;
  if (m_remainingAutoCloseSeconds <= 0)
  {
    m_autocloseTimer->stop();
    this->hide();
    this->close();
  }
  else
  {
    QAbstractButton* button = this->button(m_countdownButton);
    button->setText(QString("%1 (%2)").arg(m_originalButtonText).arg(m_remainingAutoCloseSeconds));
  }
}
