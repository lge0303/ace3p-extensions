//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_simulation_ace3p_qt_qtMessageDialog
#define smtk_simulation_ace3p_qt_qtMessageDialog

#include <QMessageBox>
#include <QMutex>
#include <QPixmap>
#include <QTime>
#include <QTimer>

#include "smtk/simulation/ace3p/qt/Exports.h"

/**
 * \brief Message Dialog with more features than the standard QMessageBox.
 * \details Message Dialog with more features than the standard QMessageBox, including:
 *              1) auto-close feature upon completion (with or without a countdown delay)
 *              2) imposing a minimum duration before auto-close to prevent "strobing" dialog on and
 *                    rapidly off
 */
class SMTKACE3PQTEXT_EXPORT qtMessageDialog : public QMessageBox
{
  Q_OBJECT

Q_SIGNALS:

public Q_SLOTS:

public:
  explicit qtMessageDialog(QWidget* parent = nullptr);
  qtMessageDialog(
    Icon icon,
    const QString& title,
    const QString& text,
    StandardButtons buttons = NoButton,
    QWidget* parent = nullptr,
    Qt::WindowFlags flags = Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
  ~qtMessageDialog();

  /** \brief Displays the Message Dialog in a blocking role. */
  int exec() override;
  /** \brief Displays the Message Dialog in a non-blocking role. */
  void show();
  /** \brief Set whether the progress dialog automatically closes after some delay. */
  void setAutoClose(bool bAutoClose);
  /** \brief Specify the delay used when closing the Message Dialog (in seconds). */
  void setAutoCloseDelay(uint delay);
  /** \brief Specify which button will be used to show autoclose countdown. */
  void setCountdownButton(StandardButton button) { m_countdownButton = button; }

private Q_SLOTS:
  void processAutoCloseTimer();

private:
  void initialize(); // common initialization
  void startAutocloseTimer();

  /** \brief Overrides show event to enforce centering on parent widget.
   *
   * In some cases, Qt is not correctly centering the dialog on its parent widget.
   * This method fixes the centering but the underlying cause remains unknown.
   */
  void showEvent(QShowEvent* event) override;

  QTimer* m_autocloseTimer;

  StandardButton m_countdownButton;
  QString m_originalButtonText;
  bool m_bAutoClose;     // tracks if autoClose will be used
  uint m_autoCloseDelay; // user-specified delay of closing after maxProgress has been reached
  uint m_remainingAutoCloseSeconds; // keeps track of remaining seconds as timer ticks down
};

#endif // smtk_simulation_ace3p_qt_qtMessageDialog
