//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/ace3p/qt/qtNerscFileItem.h"

// Plugin includes
#include "smtk/newt/qtNewtFileBrowserDialog.h"
#include "smtk/newt/qtNewtInterface.h"
#include "smtk/simulation/ace3p/qt/qtProjectRuntime.h"

// SMTK includes
#include "smtk/attribute/Item.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/extension/qt/qtBaseAttributeView.h"
#include "smtk/extension/qt/qtUIManager.h"

#include <QCheckBox>
#include <QDebug>
#include <QFrame>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QMessageBox>
#include <QPushButton>
#include <QString>
#include <QTableWidget>
#include <QTableWidgetItem>

#include <set>

namespace
{
// NERSC path info for populating job-select popup
struct ResultPath
{
  QString jobName;
  QString analysisType;
  QString path;
};

} // namespace

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class qtNerscFileItem::Internal
{
public:
  smtk::attribute::StringItemPtr m_item;

  QLineEdit* m_lineEdit = nullptr;
  QPushButton* m_browseButton = nullptr;
  QPushButton* m_jobButton = nullptr;
  bool m_useUpstream = true;    // show jobs from upstream stages (all except rfpost)
  bool m_directoryOnly = false; // show directory name (for rfpost)
  newt::qtNewtInterface* m_newt = newt::qtNewtInterface::instance();

  void setVisible(bool vis)
  {
    if (m_jobButton != nullptr)
    {
      m_jobButton->setVisible(vis);
    }
    m_lineEdit->setVisible(vis);
    if (m_browseButton != nullptr)
    {
      m_browseButton->setVisible(vis);
    }
  }

  // Populates ResultPath instance for given job record and mode
  bool getNerscPath(
    const std::shared_ptr<smtk::simulation::ace3p::JobsManifest> manifest,
    int jobIndex,
    const std::string& mode,
    ResultPath& result) const
  {
    // Skip if there was a job error
    std::string status;
    manifest->getField(jobIndex, "status", status);
    if (status == "error")
    {
      return false;
    }

    static const std::set<std::string> emagSet = { "Omega3P", "S3P", "T3P" };
    std::string analysis, name, path1, path2;
    manifest->getField(jobIndex, "analysis", analysis);

    if (mode == "emag")
    {
      if (emagSet.find(analysis) == emagSet.end())
      {
        return false;
      }

      manifest->getField(jobIndex, "job_name", name);
      manifest->getField(jobIndex, "runtime_job_folder", path1);
      manifest->getField(jobIndex, "results_subfolder", path2);

      result.jobName = QString().fromStdString(name);
      result.analysisType = QString().fromStdString(analysis);

      std::string path = m_directoryOnly ? path2 : path1 + "/" + path2;
      result.path = QString().fromStdString(path);
      return true;
    }

    if (mode == "deformed" && analysis == "TEM3P")
    {
      // Future: update job record to include deformed mesh filename
      // Current logic presumes it is included in all TEM3P jobs
      manifest->getField(jobIndex, "job_name", name);
      manifest->getField(jobIndex, "runtime_job_folder", path1);
      manifest->getField(jobIndex, "results_subfolder", path2);
      // The deformed mesh filename is hard-coded in TEM3P
      std::string path = path1 + "/" + path2 + "/DeformedVacuumMesh.ncdf";

      result.jobName = QString().fromStdString(name);
      result.analysisType = QString().fromStdString(analysis);
      result.path = QString().fromStdString(path);
      return true;
    }

    return false;
  }
};

smtk::extension::qtItem* qtNerscFileItem::createItemWidget(
  const smtk::extension::qtAttributeItemInfo& info)
{
  return new qtNerscFileItem(info);
}

qtNerscFileItem::qtNerscFileItem(const smtk::extension::qtAttributeItemInfo& info)
  : qtItem(info)
{
  this->setObjectName("NERSC File Item");
  m_internal = new qtNerscFileItem::Internal;
  m_internal->m_item = info.itemAs<smtk::attribute::StringItem>();
  this->createWidget();
}

qtNerscFileItem::~qtNerscFileItem()
{
  delete m_internal;
}

void qtNerscFileItem::clearChildWidgets()
{
  if (m_internal->m_lineEdit != nullptr)
  {
    m_internal->m_lineEdit->clear();
  }
}

void qtNerscFileItem::createWidget()
{
  smtk::attribute::ItemPtr item = m_itemInfo.item();
  auto iview = m_itemInfo.baseView();
  if (iview && !iview->displayItem(item))
  {
    return;
  }

  this->clearChildWidgets();
  this->updateUI();
}

void qtNerscFileItem::setBackground()
{
  if (
    m_internal->m_item->isSet() && m_internal->m_item->isValid() &&
    !m_internal->m_item->value().empty())
  {
    this->uiManager()->setWidgetColorToNormal(m_internal->m_lineEdit);
  }
  else
  {
    this->uiManager()->setWidgetColorToInvalid(m_internal->m_lineEdit);
  }
}

void qtNerscFileItem::updateUI()
{
  auto item = m_itemInfo.item();
  auto iview = m_itemInfo.baseView();
  if (iview && !iview->displayItem(item))
  {
    return;
  }

  if (m_widget)
  {
    delete m_widget;
  }

  m_widget = new QFrame(m_itemInfo.parentWidget());
  QHBoxLayout* layout = new QHBoxLayout(m_widget);
  layout->setMargin(0);

  // If internal item is null, the ItemView was assigned to the wrong item type
  if (m_internal->m_item == nullptr)
  {
    QLabel* label = new QLabel(item->label().c_str(), m_widget);
    layout->addWidget(label);
    QString text("Invalid NERSCDIRECTORY ItemView NOT assigned to StringItem");
    QLabel* msg = new QLabel(text, m_widget);
    layout->addWidget(msg);
    return;
  }

  // Add checkbox if optional else label
  QString textLabel = QString::fromStdString(m_internal->m_item->label());
  if (m_internal->m_item->isOptional())
  {
    QCheckBox* checkbox = new QCheckBox(m_widget);
    checkbox->setChecked(m_internal->m_item->definition()->isEnabledByDefault());
    checkbox->setText(textLabel);
    checkbox->setChecked(m_internal->m_item->localEnabledState());
    QObject::connect(checkbox, &QCheckBox::stateChanged, this, &qtNerscFileItem::onCheckBoxChanged);
    layout->addWidget(checkbox);
  }
  else
  {
    QLabel* label = new QLabel(textLabel, m_widget);
    QFontMetrics metric(label->font());
    label->setFixedWidth(metric.boundingRect(label->text()).width() + 2);
    label->setWordWrap(true);
    label->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    layout->addWidget(label);
  }

  // Add job button
  this->addJobButton();

  // Add line edit
  m_internal->m_lineEdit = new QLineEdit(m_widget);
  layout->addWidget(m_internal->m_lineEdit);

  // Get DirectoryOnly option
  bool directoryOnly = false; // default
  m_itemInfo.component().attributeAsBool("DirectoryOnly", directoryOnly);
  m_internal->m_directoryOnly = directoryOnly;
  // qDebug() << "DIRECTORY ONLY" << directoryOnly << __FILE__ << __LINE__;

  // Add browse button (except for directoryOnly option)
  m_internal->m_browseButton = nullptr;
  if (!directoryOnly)
  {
    QPushButton* browseButton = new QPushButton("Browse", m_widget);
    layout->addWidget(browseButton);
    m_internal->m_browseButton = browseButton;
  }

  QObject::connect(
    m_internal->m_lineEdit, &QLineEdit::editingFinished, this, &qtNerscFileItem::onEditingFinished);
  if (m_internal->m_browseButton != nullptr)
  {
    QObject::connect(
      m_internal->m_browseButton, &QPushButton::clicked, this, &qtNerscFileItem::onBrowse);
  }

  if (m_internal->m_item->isSet())
  {
    m_internal->m_lineEdit->setText(m_internal->m_item->value().c_str());
  }
  this->setBackground();

  bool visible = m_internal->m_item->isEnabled();
  m_internal->setVisible(visible);
}

void qtNerscFileItem::addJobButton()
{
  auto project = qtProjectRuntime::instance()->ace3pProject();
  if (project == nullptr)
  {
    return; // no project or first stage selected ==> no upstream stages
  }

  // Get widget layout so that we can add job button
  auto* layout = qobject_cast<QBoxLayout*>(m_widget->layout());
  if (layout == nullptr)
  {
    qWarning() << "Internal Error: null layout" << __FILE__ << __LINE__;
    return;
  }

  // Get job-button mode as one of:
  //   "deformed" for DeformedVacuumMesh.ncdf in upstream stage
  //   "emag" for electromagnetic results in upstream stage
  std::string mode = "emag"; // default for backward compatibility
  m_itemInfo.component().attribute("JobButton", mode);
  // qDebug() << "MODE" << mode.c_str() << __FILE__ << __LINE__;

  // Check for "Upstream" button
  bool isUpstream = true; // default
  m_itemInfo.component().attributeAsBool("Upstream", isUpstream);
  m_internal->m_useUpstream = isUpstream;
  // qDebug() << "UPSTREAM" << isUpstream << __FILE__ << __LINE__;

  if (mode == "emag")
  {
    QIcon arrowIcon(":/icons/arrow-up.svg");
    QPushButton* button = new QPushButton(arrowIcon, "", m_widget);
    button->setToolTip("Select results from electromagnetics job");
    layout->addWidget(button);
    QObject::connect(button, &QPushButton::clicked, [this, mode]() { this->onGetNerscPath(mode); });
    m_internal->m_jobButton = button;
    return;
  }

  else if (mode == "deformed")
  {
    QIcon arrowIcon(":/icons/arrow-up.svg");
    QPushButton* button = new QPushButton(arrowIcon, "", m_widget);
    button->setToolTip("Select deformed mesh from TEM3P job");
    layout->addWidget(button);
    QObject::connect(button, &QPushButton::clicked, [this, mode]() { this->onGetNerscPath(mode); });
    m_internal->m_jobButton = button;
    return;
  }

  // Any other mode string (e.g., "none", "false", "off") omits job button
}

void qtNerscFileItem::onGetNerscPath(const std::string& mode)
{
  auto project = qtProjectRuntime::instance()->ace3pProject();
  if (project == nullptr)
  {
    qWarning() << "Internal Error: No project loaded" << __FILE__ << __LINE__;
    return;
  }
  int currentStageIndex = project->currentStageIndex();
  std::shared_ptr<smtk::simulation::ace3p::JobsManifest> manifest = project->jobsManifest();

  QVector<ResultPath> results;

  // get the list of applicable job data
  int firstStageIndex = m_internal->m_useUpstream ? 0 : currentStageIndex;
  int lastStageIndex = m_internal->m_useUpstream ? currentStageIndex - 1 : currentStageIndex;

  for (int i = firstStageIndex; i <= lastStageIndex; i++)
  {
    std::shared_ptr<Stage> stage = project->stage(i);
    std::shared_ptr<smtk::attribute::Resource> attResource = stage->attributeResource();
    std::string stageID = attResource->id().toString();

    for (int j = 0; j < manifest->size(); j++)
    {
      std::string analysisID;
      manifest->getField(j, "analysis_id", analysisID);

      if (stageID == analysisID)
      {
        ResultPath result;
        if (m_internal->getNerscPath(manifest, j, mode, result))
        {
          results.push_back(result);
        }
      }
    }
  }

  // first, handle the trivial cases
  if (results.size() == 0)
  {
    QMessageBox::warning(
      this->parentWidget(), "No Jobs", "There are no applicable jobs in the Project.");
    return;
  }

  // display all Job results options for user selection
  QWidget* selectionWindow = new QWidget(qtProjectRuntime::instance()->mainWidget());
  selectionWindow->setWindowFlags(selectionWindow->windowFlags() | Qt::Window);
  selectionWindow->setAttribute(Qt::WA_DeleteOnClose);
  selectionWindow->setWindowModality(Qt::WindowModal);
  selectionWindow->setWindowTitle("Job Results");
  QVBoxLayout* mainLayout = new QVBoxLayout();

  QTableWidget* tableWidget = new QTableWidget(selectionWindow);
  tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
  tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
  tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
  tableWidget->setColumnCount(3);
  tableWidget->setRowCount(results.size());
  tableWidget->setHorizontalHeaderLabels(QStringList{ "Job Name", "ACE3P Code", "Results Path" });
  tableWidget->verticalHeader()->setVisible(false);
  tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
  tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
  tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
  for (int i = 0; i < results.size(); i++)
  {
    tableWidget->setItem(i, 0, new QTableWidgetItem(results[i].jobName));
    tableWidget->setItem(i, 1, new QTableWidgetItem(results[i].analysisType));
    tableWidget->setItem(i, 2, new QTableWidgetItem(results[i].path));
  }
  mainLayout->addWidget(tableWidget);

  QHBoxLayout* buttonsLayout = new QHBoxLayout();

  // setup the Cancel button
  QPushButton* cancelButton = new QPushButton(selectionWindow);
  cancelButton->setText(tr("Cancel"));
  QObject::connect(
    cancelButton, &QPushButton::clicked, [selectionWindow]() { selectionWindow->close(); });
  buttonsLayout->addWidget(cancelButton);

  buttonsLayout->addSpacerItem(
    new QSpacerItem(10, cancelButton->height(), QSizePolicy::MinimumExpanding));

  // setup the Select button
  QPushButton* selectButton = new QPushButton(selectionWindow);
  selectButton->setText(tr("Select"));
  selectButton->setEnabled(false); // noting selected to start
  QObject::connect(tableWidget, &QTableWidget::itemSelectionChanged, [tableWidget, selectButton]() {
    int numSelected = tableWidget->selectedItems().size();
    selectButton->setEnabled(numSelected > 0);
  });
  QObject::connect(
    selectButton, &QPushButton::clicked, [this, results, tableWidget, selectionWindow]() {
      QList<QTableWidgetItem*> items = tableWidget->selectedItems();
      if (items.empty())
      {
        qWarning() << "No row was selected.";
        return;
      }
      int index = items[0]->row();
      this->m_internal->m_lineEdit->setText(results[index].path);
      this->onEditingFinished();

      selectionWindow->close();
    });
  buttonsLayout->addWidget(selectButton);
  mainLayout->addLayout(buttonsLayout);

  selectionWindow->setLayout(mainLayout);
  selectionWindow->resize(640, 320);
  selectionWindow->show();
}

void qtNerscFileItem::onBrowse()
{
  // First check if user is signed in to NERSC
  if (!m_internal->m_newt->isLoggedIn())
  {
    QMessageBox::information(
      this->parentWidget(), "Not Signed in", "You must first log into NERSC from the application.");
    return;
  }

  // Instantiate browser
  newt::qtNewtFileBrowserDialog dialog(this->parentWidget());
  QObject::connect(
    &dialog, &newt::qtNewtFileBrowserDialog::applyPath, this, &qtNerscFileItem::onDialogApply);
  dialog.exec();
}

void qtNerscFileItem::onDialogApply(const QString& path)
{
  if (m_internal->m_item == nullptr)
  {
    return;
  }

  if (path.toStdString() == m_internal->m_item->value())
  {
    return;
  }

  bool isChanged = m_internal->m_item->setValue(path.toStdString());
  if (!isChanged)
  {
    qWarning() << "Failed to set NERSC Directory (path) to" << path;
    return;
  }

  m_internal->m_lineEdit->setText(path);
  this->setBackground();
  Q_EMIT this->modified();
}

void qtNerscFileItem::onEditingFinished()
{
  if (m_internal->m_item == nullptr)
  {
    return;
  }

  auto inputString = m_internal->m_lineEdit->text().toStdString();
  if (m_internal->m_item->isSet())
  {
    if (inputString == m_internal->m_item->value())
    {
      return;
    }
  }
  else if (inputString.empty())
  {
    return;
  }

  bool isChanged = m_internal->m_item->setValue(inputString);
  if (!isChanged)
  {
    qWarning() << "Failed to set NERSC Directory (path) to" << inputString.c_str();
  }

  this->setBackground();
  Q_EMIT this->modified();
}

void qtNerscFileItem::onCheckBoxChanged(int state)
{
  bool enable = state != 0;
  auto item = m_internal->m_item;
  if (enable == item->localEnabledState())
  {
    return;
  }

  item->setIsEnabled(enable);
  m_internal->setVisible(enable);
  auto* iview = m_itemInfo.baseView();
  if (iview)
  {
    iview->valueChanged(item);
  }
  Q_EMIT this->modified();
}

} // namespace ace3p
} // namespace simulation
} // namespace smtk
