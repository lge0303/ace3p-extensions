//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/ace3p/qt/qtNewProjectMeshPage.h"

#include "smtk/simulation/ace3p/qt/ui_qtNewProjectMeshPage.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

namespace smtk
{
namespace simulation
{
namespace ace3p
{

qtNewProjectMeshPage::qtNewProjectMeshPage(QWidget* parent)
  : Superclass(parent)
  , ui(new Ui::qtNewProjectMeshPage)
  , m_fileDialog(new QFileDialog(this))
{
  this->ui->setupUi(this);
  this->setButtonText(QWizard::CommitButton, "Create Project");
  this->setCommitPage(true);

  // Set default field name
  this->ui->m_locationEdit->setText("");

  // Configure file dialog
  m_fileDialog->setLabelText(QFileDialog::Accept, "Accept");
  m_fileDialog->setFileMode(QFileDialog::ExistingFile);
  m_fileDialog->setNameFilter("Exodus Files (*.ex? *.gen);;NetCDF Files (*.ncdf);;All Files (*)");
  m_fileDialog->setDirectory(QDir::homePath());

  // Setup fields
  this->registerField("project.meshfile", this->ui->m_locationEdit);

  // Setup connections
  QObject::connect(
    this->ui->m_browseButton,
    &QAbstractButton::clicked,
    this,
    &qtNewProjectMeshPage::browseLocation);
}

QString qtNewProjectMeshPage::meshFileDirectory() const
{
  return m_fileDialog->directory().absolutePath();
}

void qtNewProjectMeshPage::setMeshFileDirectory(const QString& path)
{
  m_meshFileDirectory.setPath(path);
  this->ui->m_locationEdit->setText(path);
  m_fileDialog->setDirectory(path);
}

bool qtNewProjectMeshPage::validatePage()
{
  // Check the location
  QString location = this->ui->m_locationEdit->text();
  if (location.isEmpty())
  {
    QMessageBox::critical(this, "Missing Input", "No model file (.gen) is specified.");
    return false;
  }

  // Check that the location (file) exists
  QFileInfo fileInfo(location);
  if (!fileInfo.exists() || !fileInfo.isFile())
  {
    QMessageBox::critical(this, "Not Found", QString("Model file not found: %1").arg(location));
    return false;
  }

  // Update field data
  this->setField("project.meshfile", location);

  return true;
}

void qtNewProjectMeshPage::browseLocation()
{
  QString location = this->ui->m_locationEdit->text();
  if (location.isEmpty())
  {
    if (m_meshFileDirectory.isEmpty())
    {
      location = QDir::homePath();
    }
    else
    {
      location = m_meshFileDirectory.absolutePath();
    }
  }
  else
  {
    QFileInfo fileInfo(location);
    if (!fileInfo.exists())
    {
      location = QDir::homePath();
    }
    else if (fileInfo.isFile())
    {
      location = fileInfo.dir().absolutePath();
    }
  }

  m_fileDialog->setDirectory(location);
  int result = m_fileDialog->exec();
  if (result == QDialog::Accepted)
  {
    location = m_fileDialog->selectedFiles().at(0);
    this->ui->m_locationEdit->setText(location);
  }
}

} // namespace ace3p
} // namespace simulation
} // namespace smtk
