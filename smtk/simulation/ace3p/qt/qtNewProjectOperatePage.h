//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_simulation_ace3p_qt_qtNewProjectOperatePage_h
#define smtk_simulation_ace3p_qt_qtNewProjectOperatePage_h

#include <QWizardPage>

#include "smtk/PublicPointerDefs.h"

#include "smtk/simulation/ace3p/qt/Exports.h"

#include "smtk/operation/Operation.h"

#include <QString>

class QListWidget;

/// \brief Wizard page for running the ace3p project Create & Write operations.

namespace smtk
{
namespace simulation
{
namespace ace3p
{
class Project;
class qtOperationLauncher;

class SMTKACE3PQTEXT_EXPORT qtNewProjectOperatePage : public QWizardPage
{
  Q_OBJECT
  using Superclass = QWizardPage;

public:
  qtNewProjectOperatePage(QWidget* parent = nullptr);
  ~qtNewProjectOperatePage();

  /// Project manager *must* be set before calling operate method
  void setProjectManager(std::shared_ptr<smtk::project::Manager> projectManager)
  {
    m_projectManager = projectManager;
  }

  bool operate();

  std::shared_ptr<Project> project() const { return m_project; }

Q_SIGNALS:
  void operationComplete(bool ok);

protected Q_SLOTS:
  void onCreateResult(smtk::operation::Operation::Result);
  void onWriteResult(smtk::operation::Operation::Result);

protected:
  bool checkResult(const smtk::operation::Operation::Result& result, const QString& operationName);

  std::weak_ptr<smtk::project::Manager> m_projectManager;
  std::shared_ptr<Project> m_project;
  qtOperationLauncher* m_launcher;
  QListWidget* m_listWidget;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
