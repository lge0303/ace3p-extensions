//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_simulation_ace3p_qt_qtNewProjectWizard_h
#define smtk_simulation_ace3p_qt_qtNewProjectWizard_h

#include "smtk/simulation/ace3p/Project.h"
#include "smtk/simulation/ace3p/qt/Exports.h"

#include "smtk/PublicPointerDefs.h"

#include <QWizard>

/** \brief A small user wizard for creating projects.
 *
 * Simplifies the process of creating a new project as much as practical
 */

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class qtNewProjectMeshPage;
class qtNewProjectNamePage;
class qtNewProjectOperatePage;

class SMTKACE3PQTEXT_EXPORT qtNewProjectWizard : public QWizard
{
  Q_OBJECT
  using Superclass = QWizard;

public:
  qtNewProjectWizard(QWidget* parent = nullptr);
  ~qtNewProjectWizard() = default;

  /// Project manager *must* be set before starting the wizard
  void setProjectManager(std::shared_ptr<smtk::project::Manager> projectManager);

  QString projectLocation() const;
  void setProjectLocation(const QString& path);

  QString meshFileDirectory() const;
  void setMeshFileDirectory(const QString& path);

  std::shared_ptr<Project> project() const;

protected Q_SLOTS:
  void pageChanged(int id);

protected:
  qtNewProjectNamePage* m_namePage;
  qtNewProjectMeshPage* m_meshPage;
  qtNewProjectOperatePage* m_operatePage;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
