//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/ace3p/qt/qtStagesWidget.h"
#include "smtk/simulation/ace3p/qt/qtStagesModel.h"
#include "smtk/simulation/ace3p/qt/ui_qtStagesWidget.h"

#include "smtk/simulation/ace3p/Project.h"
#include "smtk/simulation/ace3p/qt/qtProjectRuntime.h"

#include "smtk/PublicPointerDefs.h"
#include "smtk/attribute/Resource.h"

// delete this later - TODO (JOHN)
#include "smtk/simulation/ace3p/testing/cxx/randomJobCreator.h"

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QItemSelectionModel>
#include <QMessageBox>
#include <QTimer>
#include <QVariant>

namespace smtk
{
namespace simulation
{
namespace ace3p
{

qtStagesWidget::qtStagesWidget(QWidget* parentWidget)
  : QWidget(parentWidget)
  , m_stages_model(new qtStagesModel(this))
  , ui(new Ui::qtStagesWidget)
{
  // initialize the UI
  this->ui->setupUi(this);

  // setup stages table
  this->ui->m_stagesTable->setModel(m_stages_model);
  this->ui->m_stagesTable->setSelectionBehavior(QAbstractItemView::SelectRows);
  this->ui->m_stagesTable->setSelectionMode(QAbstractItemView::SingleSelection);
  this->ui->m_stagesTable->setEditTriggers(QAbstractItemView::DoubleClicked);
  this->ui->m_stagesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  this->ui->m_stagesTable->show();

  // windows only stylesheet declaration to get the horizontal header cells to properly draw their
  //    border lines
  if (QSysInfo::windowsVersion() == QSysInfo::WV_WINDOWS10)
  {
    this->ui->m_stagesTable->horizontalHeader()->setStyleSheet("QHeaderView::section{"
                                                               "border-top:0px solid #D8D8D8;"
                                                               "border-left:0px solid #D8D8D8;"
                                                               "border-right:1px solid #D8D8D8;"
                                                               "border-bottom: 1px solid #D8D8D8;"
                                                               "background-color:white;"
                                                               "}"
                                                               /*
      "QTableCornerButton::section{"
        "border-top:0px solid #D8D8D8;"
        "border-left:0px solid #D8D8D8;"
        "border-right:1px solid #D8D8D8;"
        "border-bottom: 1px solid #D8D8D8;"
        "background-color:white;"
      "}"
      */
    );
  }
}

void qtStagesWidget::onStageAdded(int stageNumber)
{
  m_stages_model->addStage(stageNumber);
  QCoreApplication::processEvents(); // needed?

  this->ui->m_stagesTable->selectRow(stageNumber);
  qDebug() << __FILE__ << __LINE__ << "Added & selected stage" << stageNumber;
}

void qtStagesWidget::onUpdateProjectCurrentStage(int currentStage)
{
  // qDebug() << __FILE__ << __LINE__ << "onUpdateProjectCurrentStage";
  this->ui->m_stagesTable->selectRow(currentStage);
  Q_EMIT this->selectStageClicked(currentStage);
}

void qtStagesWidget::setProject(smtk::project::ProjectPtr project)
{
  m_stages_model->setProject(project);
}

void qtStagesWidget::unsetProject()
{
  m_stages_model->setProject(nullptr);
}

void qtStagesWidget::setGUIRowSelection(int index)
{
  this->ui->m_stagesTable->selectRow(index);
}

void qtStagesWidget::on_pushButton_Add_clicked()
{
  smtk::simulation::ace3p::qtProjectRuntime* project = qtProjectRuntime::instance();
  if (!project->ace3pProject())
  {
    QMessageBox::warning(this, "Add Warning", "You must first open/create a project.");
    return;
  }

  Q_EMIT addStageClicked();
}

int qtStagesWidget::getViewSelectedRowIndex()
{
  QItemSelectionModel* pSelection = this->ui->m_stagesTable->selectionModel();
  QModelIndexList rowList = pSelection->selectedRows();
  if (rowList.isEmpty())
  {
    return -1;
  }

  return rowList[0].row();
}

void qtStagesWidget::on_pushButton_Delete_clicked()
{
  int selectionIndex = this->getViewSelectedRowIndex();
  if (selectionIndex == -1)
  {
    QMessageBox::warning(this, "Selection Warning", "You must first select a stage from the list.");
    return;
  }

  smtk::simulation::ace3p::qtProjectRuntime* project = qtProjectRuntime::instance();
  if (project->ace3pProject()->numberOfStages() < 2)
  {
    QMessageBox::warning(
      this, "Remove Warning", "You cannot remove the last stage from a project.");
    return;
  }

  // start the process of Job (and Stage) deletion
  deleteStageJobs(selectionIndex);
}

void qtStagesWidget::deleteStageJobs(int index)
{
  auto project = qtProjectRuntime::instance()->ace3pProject();

  std::shared_ptr<Stage> stage = project->stage(index);
  std::shared_ptr<smtk::attribute::Resource> attResource = stage->attributeResource();
  std::string stageID = attResource->id().toString();
  std::shared_ptr<smtk::simulation::ace3p::JobsManifest> manifest = project->jobsManifest();

  // get the list of jobIDs to be deleted
  QVector<QString> stageJobIDs;
  for (int i = 0; i < manifest->size(); i++)
  {
    std::string analysisID;
    manifest->getField(i, "analysis_id", analysisID);

    // add the job to the deletion list, if the UUIDs match
    if (stageID == analysisID)
    {
      std::string jobID;
      manifest->getField(i, "job_id", jobID);

      stageJobIDs.push_back(QString().fromStdString(jobID));
    }
  }

  // verify that deletion is desired
  QString pluralization = stageJobIDs.count() == 1 ? "" : "s";
  QMessageBox confirmationCheck(this);
  confirmationCheck.setWindowTitle("Delete Records?");
  confirmationCheck.setText(QString("This will delete the stage and %1 job record%2.\n\n"
                                    "Are you sure you want to continue?")
                              .arg(stageJobIDs.count())
                              .arg(pluralization));
  /* QPushButton* cancelButton = */ confirmationCheck.addButton("Cancel", QMessageBox::NoRole);
  QPushButton* deleteButton = confirmationCheck.addButton("Delete Stage", QMessageBox::YesRole);
  confirmationCheck
    .exec(); // this is a blocking function call (as opposed to show(), which doesn't block)
  if (confirmationCheck.clickedButton() != deleteButton)
  {
    return;
  }

  // do the actual job deletion (NOTE: the manifest is written after each delete)
  for (int i = 0; i < stageJobIDs.size(); i++)
  {
    Q_EMIT this->deleteJob(stageJobIDs[i]);
  }
  QCoreApplication::processEvents(); // force all signals to be processed

  // delete the stage
  Q_EMIT this->deleteStageClicked(index); // removes stage from project
  m_stages_model->removeStage(index);     // removes stage from QTableView
}

void qtStagesWidget::on_m_stagesTable_clicked(const QModelIndex& index)
{
  if (index.row() != m_stages_model->projectCurrentStage())
  {
    Q_EMIT this->selectStageClicked(index.row());
  }
}

} // namespace ace3p
} // namespace simulation
} // namespace smtk
